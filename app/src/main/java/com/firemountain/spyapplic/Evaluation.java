package com.firemountain.spyapplic;

class Evaluation {
    private String name;

    private Evaluation(String name) {
        this.name = name;
    }

    static final Evaluation[] evaluations = {
            new Evaluation("Всегда"),
            new Evaluation("Иногда"),
            new Evaluation("Ни разу")
    };

    String getName() {
        return name;
    }

}
