package com.firemountain.spyapplic;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends Activity {

    private static int countEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        countEntries = 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void onClickEnterButton(View v){

        EditText loginEditText = (EditText) findViewById(R.id.loginEditText);
        EditText passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        String login = loginEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        System.out.println("login: " + login);
        System.out.println("password: " + password);

        if (!login.equals("admin") || !password.equals("admin")){

            if (countEntries == 2){
                finishAffinity();

            } else {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Внимание!");
                builder.setMessage("Не верный логин или пароль");

                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
            countEntries++;
        } else {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
