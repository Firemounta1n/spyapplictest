package com.firemountain.spyapplic;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends Activity {

    private static HashMap<Integer, ArrayList<Integer>> answerHashMap;

    SharedPreferences hashMapPref;

    final String SAVED_HASH_MAP = "saved_hash_map";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String jsonString;

        hashMapPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        jsonString = hashMapPref.getString(SAVED_HASH_MAP, "");

        //Toast.makeText(this, "Text loaded", Toast.LENGTH_SHORT).show();
        answerHashMap = new Gson().fromJson(jsonString, new TypeToken<HashMap<Integer, ArrayList<Integer>>>(){}.getType());
    }

    public void onClickQuestionnaireList(View v) {
        Intent intent = new Intent(MainActivity.this, QuestionnaireListActivity.class);
        startActivity(intent);
    }

    public void onClickAddNewQuestionnaire(View v) {
        Intent intent = new Intent(MainActivity.this, QuestionnaireActivity.class);
        startActivity(intent);
    }

    public void onClickCleanList(View v) {
        if (answerHashMap != null) {
            answerHashMap.clear();
        }

        hashMapPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor ed = hashMapPref.edit();
        ed.clear();
        ed.apply();
        //Toast.makeText(this, "Text saved", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
