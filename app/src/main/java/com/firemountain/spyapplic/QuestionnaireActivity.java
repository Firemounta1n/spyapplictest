package com.firemountain.spyapplic;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class QuestionnaireActivity extends Activity {

    private static HashMap<Integer, ArrayList<Integer>> answerHashMap;
    private static boolean notAllCheck;
    private static int questionPosition;

    SharedPreferences hashMapPref;

    final String SAVED_HASH_MAP = "saved_hash_map";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);

        Intent intent = getIntent();
        questionPosition = intent.getIntExtra("QUESTION_POSITION", -1);

        notAllCheck = false;
        ArrayList<Integer> savedAnswers = new ArrayList<>();

        String jsonString;

        hashMapPref = PreferenceManager.getDefaultSharedPreferences(QuestionnaireActivity.this);
        jsonString = hashMapPref.getString(SAVED_HASH_MAP, "");
        //System.out.println("jsonString: " + jsonString);

        //Toast.makeText(this, "Text loaded", Toast.LENGTH_SHORT).show();
        answerHashMap = new Gson().fromJson(jsonString, new TypeToken<HashMap<Integer, ArrayList<Integer>>>(){}.getType());

        if (answerHashMap == null){
            answerHashMap = new HashMap<>();
        }

        //System.out.println("sparseArray: " + hashMap);
        //System.out.println("answerHashMap: " + answerHashMap);

        if (questionPosition != -1) {
            savedAnswers = answerHashMap.get(questionPosition);
        }

        if (savedInstanceState != null) {
            savedAnswers = savedInstanceState.getIntegerArrayList("saved_answers");
        }

        ArrayList<String> questionName = new ArrayList<>();
        ArrayList<String> questionDescription = new ArrayList<>();
        ArrayList<String> evaluationName = new ArrayList<>();

        for (int i = 0; i < Question.questions.length; i++) {
            questionName.add(i, Question.questions[i].getName());
            questionDescription.add(i, Question.questions[i].getDescription());
        }

        for (int i = 0; i < Evaluation.evaluations.length; i++) {
            evaluationName.add(i, Evaluation.evaluations[i].getName());
        }

        ListView paramsListView = (ListView) findViewById(R.id.params_list_view);
        final QuestionnaireAdapter adapter = new
                QuestionnaireAdapter(this, questionName, questionDescription, evaluationName, savedAnswers);
        paramsListView.setAdapter(adapter);
    }

    public void onClickSave(View v) {

        ArrayList<Integer> checkedAnswers = QuestionnaireAdapter.getCheckedAnswers();

        for (int checkedAnswer : checkedAnswers) {
            if (checkedAnswer == -1) {
                notAllCheck = true;
                break;
            } else {
                notAllCheck = false;
            }
        }

        if (notAllCheck) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(QuestionnaireActivity.this);
            builder.setTitle("Внимание!");
            builder.setMessage("Необходимо полностью заполнить анкету");

            builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        } else {
            if (questionPosition != -1) {

                Intent intent = new Intent(QuestionnaireActivity.this, QuestionnaireListActivity.class);

                answerHashMap.remove(questionPosition);
                answerHashMap.put(questionPosition, checkedAnswers);
                startActivity(intent);
            } else {
                Intent intent = new Intent(QuestionnaireActivity.this, MainActivity.class);

                answerHashMap.put(answerHashMap.size(), checkedAnswers);
                startActivity(intent);
            }

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
            Type type = new TypeToken<HashMap<Integer, ArrayList<Integer>>>(){}.getType();
            String json = gson.toJson(answerHashMap, type);

            hashMapPref = PreferenceManager.getDefaultSharedPreferences(QuestionnaireActivity.this);
            SharedPreferences.Editor ed = hashMapPref.edit();

            ed.putString(SAVED_HASH_MAP, json);
            ed.apply();
            //Toast.makeText(this, "Text saved", Toast.LENGTH_SHORT).show();
            //System.out.println("sparse: " + answerHashMap);
            //System.out.println("json: " + json);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putIntegerArrayList("saved_answers", QuestionnaireAdapter.getCheckedAnswers());
    }
}
