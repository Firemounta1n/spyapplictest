package com.firemountain.spyapplic;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

class QuestionnaireAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> paramName;
    private final ArrayList<String> paramDescription;
    private final ArrayList<String> ratingEvaluation;
    private static ArrayList<Integer> checkedAnswers;

    QuestionnaireAdapter(Activity context,
                         ArrayList<String> paramName, ArrayList<String> paramDescription, ArrayList<String> ratingEvaluation, ArrayList<Integer> savedAnswers) {
        super(context, R.layout.questionnaire_list_view, paramName);
        this.context = context;
        this.paramName = paramName;
        this.paramDescription = paramDescription;
        this.ratingEvaluation = ratingEvaluation;

        if (savedAnswers.size() != 0) {
            checkedAnswers = savedAnswers;
        } else {
            checkedAnswers = new ArrayList<>();

            for (int i = 0; i < paramName.size(); i++) {
                checkedAnswers.add(i, -1);
            }
        }
    }

    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.questionnaire_list_view, null, true);

        TextView textName = (TextView) rowView.findViewById(R.id.textName);
        TextView textDescription = (TextView) rowView.findViewById(R.id.textDescription);
        RadioGroup radioGroup = (RadioGroup) rowView.findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1: // ничего не выбрано
                        checkedAnswers.set(position, -1);
                        break;
                    case R.id.radioAlways:
                        checkedAnswers.set(position, 0);
                        break;
                    case R.id.radioSometimes:
                        checkedAnswers.set(position, 1);
                        break;
                    case R.id.radioNever:
                        checkedAnswers.set(position, 2);
                        break;

                    default:
                        break;
                }
            }
        });

        RadioButton radioAlways = (RadioButton) rowView.findViewById(R.id.radioAlways);
        RadioButton radioSometimes = (RadioButton) rowView.findViewById(R.id.radioSometimes);
        RadioButton radioNever = (RadioButton) rowView.findViewById(R.id.radioNever);

        textName.setText(paramName.get(position));
        textDescription.setText(paramDescription.get(position));
        radioAlways.setText(ratingEvaluation.get(0));
        radioSometimes.setText(ratingEvaluation.get(1));
        radioNever.setText(ratingEvaluation.get(2));

        if ((checkedAnswers.get(position)) == -1) {

            radioGroup.clearCheck();
        } else {
            radioGroup.check(radioGroup.getChildAt(checkedAnswers.get(position)).getId());
        }
        return rowView;
    }

    static ArrayList<Integer> getCheckedAnswers(){
        return checkedAnswers;
    }

}
