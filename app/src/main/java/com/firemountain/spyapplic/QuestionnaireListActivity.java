package com.firemountain.spyapplic;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

public class QuestionnaireListActivity extends ListActivity {

    SharedPreferences hashMapPref;

    final String SAVED_HASH_MAP = "saved_hash_map";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String jsonString;

        hashMapPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        jsonString = hashMapPref.getString(SAVED_HASH_MAP, "");

        //Toast.makeText(this, "Text loaded", Toast.LENGTH_SHORT).show();

        HashMap<Integer, ArrayList<Integer>> answerHashMap = new Gson().fromJson(jsonString, new TypeToken<HashMap<Integer, ArrayList<Integer>>>() {
        }.getType());

        if (answerHashMap == null){
            answerHashMap = new HashMap<>();
        }

        ArrayList<String> questionnaireArray = new ArrayList<>();

        for (int i = 0; i < answerHashMap.size(); i++) {
            String questionnaireName = "Анкета №" + (i + 1);
            questionnaireArray.add(questionnaireName);
        }


        ListView listQuestionnaires = getListView();
        ArrayAdapter<String> listAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                questionnaireArray);
        listQuestionnaires.setAdapter(listAdapter);
    }

    @Override
    public void onListItemClick(ListView listView,
                                View itemView,
                                int position,
                                long id) {
        Intent intent = new Intent(QuestionnaireListActivity.this, QuestionnaireActivity.class);
        intent.putExtra("QUESTION_POSITION", position);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(QuestionnaireListActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}
